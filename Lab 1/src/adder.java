/*Name:Christopher Mahon
Email: christopher.mahon2@student.dit.ie
Student Code: C12380621
Module: Network Programming
Assignment: Lab 1
Class Name: adder
Class takes in 2 command line arguments and add them together
If an error occurs, it should inform the user of an error*/

package ie.dit.student.Mahon.Christopher;

public class adder
{ //Start of the Class designed to add 2 numbers together
	public static void main(String[] args)
	{//Beginning of the main function in adder
		if (args.length == 2)
		{ //This If Statement checks if there are enough arguments provided
			int int1 = 0;
			int int2 = 0;
			
			try
			{ //This Try Block is to test if the first argument is the correct format
				int1 = Integer.parseInt(args[0]); //Built in integer function for converting a string to an equivalent integer
				
			}
			catch (NumberFormatException e)
			{ //Catches if the conversion to int fails on the first argument
				System.out.println("Conversion failed on first Argument");
				System.exit(-2); //Error code for bad type of input
				
			} //End Try/Catch Blocks for correct input
			
			try
			{ //This Try Block is to test if the second argument is the correct format
				int2 = Integer.parseInt(args[1]);
				
			}
			catch (NumberFormatException e)
			{ //Catches if the conversion to int fails on the second argument
				System.out.println("Conversion failed on second Argument");
				System.exit(-3); //Error code for bad type of input
				
			} //End Try/Catch Blocks for correct input
	
			if ((long)int1+int2 < 2147483647)//Checks to see if the addition will result in a number too large for an int
			{//if statement is to test to make sure that the result is not too big for an integer
				int sum = int1 + int2;
				System.out.println("The Final sum is " +sum);
				System.exit(0);//Exit code for everything working okay
				
			}
			else
			{//If it is too big, itll display an error on the command line
				System.out.println("The numbers supplied were larger then desired.\n"+
						"The Final sum can not be calculated");
				System.exit(-4); //Error code for the numbers exceeding the limits of an integer
				
			}//End If that checks for int size
			
		}
		else
		{
			System.out.println("Error, input an argument");
			System.exit(-1); //Error code for not enough arguments
			
		} //End If/Else that checks if there are enough args
	} //End Main
} //End of Adder
