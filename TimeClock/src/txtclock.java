/**Assignment to take in variables from the command line and convert them to text. Or access the NTP server and convert the time given from there
Name:Christopher Mahon
Email: christopher.mahon2@student.dit.ie
Student Code: C12380621
Module: Network Programming
Assignment: NCPA
Class Name: Timeclock
Version: 1.0.1
*/

package ie.dit.student.Mahon.Christopher;

import java.net.Socket;
import java.util.Calendar;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.IOException;

public class txtclock
{
	public static void main(String[] args)
	{/**This is to convert the time input or pulled from the NTP server into a text string
			Beginning of the main in TimeClock
		*/
		
		//Declarations
		int Err=0;
		
		//Code
		if (Err==-1)
		{//Checking if the FileTransfer function succeeds
			System.out.println("Language Files not found, program can not run");
			System.exit(-11);
		}//Will cause the program to completely fail if it breaks
		
		if (args.length==1 || (args.length==2 && args[1].equals("AM")))
		{//Checking to see if the argument is in the 24 hour Clock or is in the AM format
			
			//Main Program
			Err = ConvertTime(args[0], 0); //Running my function which converts the time
			if (Err == -1)
			{//Checking for error codes from ConvertTime
				System.out.println("The program has ran into an error while converting the time");
				System.exit(-3);
					
			}
			else if (Err == -2)
			{//Checks if the parameters entered were too large
				System.out.println("The hour or minutes provided were too large");
				System.exit(-5);
					
			}
			else if (Err == -3)
			{//Checks if the Parameters entered were negative
				System.out.println("Cant have a negative time");
				System.exit(-6);
					
			}
			
		}
		else if (args.length==2)
		{//Checks if there are is a PM parameter
			if (args[1].equals("PM"))
			{
				Err = ConvertTime(args[0], 1); //Running my function which converts the time
				
				if (Err == -1)
				{//Checking for error codes from ConvertTime
					System.out.println("The program has ran into an error while converting the time");
					System.exit(-3);
					
				}
				else if (Err == -2)
				{//Checks if the parameters entered were too large
					System.out.println("The hour or minutes provided were too large");
					System.exit(-5);
					
				}
				else if (Err == -3)
				{//Checks if the Parameters entered were negative
					System.out.println("Cant have a negative time");
					System.exit(-6);
					
				}//End of if/else statement that checks the return from Convert Time
			
			}//End of check for 2 parameter being PM
			else
			{//If the second parameter is not AM or PM, sends out an error
				System.out.println("Second Argument is not in the right format");
				System.exit(-2);
				
			}
		}
		else if (args.length==0)
		{//If there are no arguments supplied, by default itll access the NTP server
			//Declarations
			String TimeString;
			
			//Code
			TimeString = PullingNTPTime();
			Err = ConvertTime(TimeString, 0); //Running my function which converts the time
			if (Err == -1)
			{//Checking for error codes from ConvertTime
				System.out.println("The program has ran into an error while converting the time");
				System.exit(-3);
			}
			else if (Err == -2)
			{//Checks if the parameters entered were too large
				System.out.println("The hour or minutes provided were too large");
				System.exit(-5);
				
			}
			else if (Err == -3)
			{//Checks if the Parameters entered were negative
				System.out.println("Cant have a negative time");
				System.exit(-6);
				
			}//End of if statement that checks the return from Convert Time
			
		}//End of the else if statement for when the program gets the time from NTP
		else if (args.length>3)
		{//Once it gets more then 2 arguments, it sends out an error
			System.out.println("Too many arguments supplied");
			System.exit(-1);
			
		}
		else
		{//Else just to make sure that there isnt some uncaught problem
			System.out.println("Technically shouldnt be able to arrive here without a negative amount arguments!");
			System.exit(-10);
			
		}//End of IF/Else for checking the number of arguments supplied
		
		System.exit(0);
	}//End of Main function
	
	
	
	public static Integer ConvertTime(String timeString, int PMflag)
	{/**Converts time to the text equivalent(In Progress)*/
		//Declarations
		String[] TimeStr = timeString.split(":"); //Splitting the string into the two components
		//Contains all the words required for the time conversion
		String[] TimeWords = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", 
								"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty", 
								"twenty one", "twenty two", "twenty three", "twenty four", "twenty five", 
								"twenty six", "twenty seven" ,"twenty eight", "twenty nine", "thirty"}; 
		int Seg1 = 0; //Segment 1 of the time string
		int Seg2 = 0; //Segment 2 of the time string
		String OutputString = "";
		
		
		//Main Function
		try
		{ //This Try Block is to test if the first segment is the correct format
			Seg1 = Integer.parseInt(TimeStr[0]); //Built in integer function for converting a string to an equivalent integer
			Seg2 = Integer.parseInt(TimeStr[1]);
			
		}
		catch (NumberFormatException e)
		{ //Catches if the conversion to int fails on the first segment
			System.out.println("Conversion failed on the time string");
			return(-1); //Error code for bad type of input
			
		} //End Try/Catch Blocks for correct time
		
		if (PMflag==1)
		{//Checks if the the Segment needs to be increased by 12 in the case of a PM parameter
			Seg1+=12;
			
		}//End If
		
		if(Seg1 > 24 || Seg2 >59)
		{//Checks is the Arguments are within the valid parameters
			return(-2);
		}
		else if (Seg1<0 || Seg2 <0)
		{//Checks if the numbers supplied are negative
			return(-3);
		}//End of if if the parameters are not valid
		
		
		 if (Seg1 == 0 || (Seg1 == 23 && Seg2 > 30))
		{//Checks if the hour is set zero (ie its midnight)
			if (Seg1 == 23 && Seg2 > 30)
			{//Checks if the time is almost at midnight
				OutputString = "It is " + TimeWords[60-Seg2] + " minute(s) to midnight";
			}
			else if (Seg2 > 30)
			{//Checks if the Minutes are greater then 30
				OutputString = "It is " + TimeWords[60-Seg2] + " minute(s) to " + TimeWords[Seg1+1];
			}
			else if(Seg2 == 0)
			{//Checks if the minutes are equal to zero
				OutputString = "It is Midnight";
			}
			else
			{//Executes when the minutes are less then 30
				OutputString = "It is " + TimeWords[Seg2] + " minute(s) past midnight" + TimeWords[Seg1];
			}
		}
		else if(Seg1 > 12)
		{//Will execute if the hour is greater then 12
			Seg1-=12;
			if(Seg2 == 0)
			{//Checks if the minutes are equal to zero
				OutputString = "It is " + TimeWords[Seg1] + " O'Clock";
			}
			else if (Seg2 > 30)
			{//Checks if the minutes are greater then 30
				OutputString = "It is " + TimeWords[60-Seg2] + " minute(s) to " + TimeWords[Seg1+1] + " in the evening";
			}
			else
			{//Otherwise the minutes are less then 30
				OutputString = "It is " + TimeWords[Seg2] + " minute(s) past " + TimeWords[Seg1] + " in the evening";
			}	
		}
		else
		{//Will execute if the hour is less then 12
			if(Seg2 == 0)
			{//Checks if the minutes are equal to zero
				OutputString = "It is " + TimeWords[Seg1] + " O'Clock";
			}
			else if (Seg2 > 30)
			{//Checks if the minutes are greater then 30
				OutputString = "It is " + TimeWords[60-Seg2] + " minute(s) to " + TimeWords[Seg1+1] + " in the morning";
			}
			else
			{//Otherwise the minutes are less then 30
				OutputString = "It is " + TimeWords[Seg2] + " minute(s) past " + TimeWords[Seg1] + " in the morning";
			}	
		}
		
		System.out.println(OutputString);
		return 0;
	}//End of the Convert Time functions
	
	
	
	
	public static String PullingNTPTime()
	{//Pulling down the time from the NTP server or failing that, pulling it from the computer
		//Declarations
		String OutTimeString = "00:00"; //This is the string that will be returned from the NTP server
		
		Socket NTPSocket = null; //Creating a blank Socket to communicate with the NTP server
		
		//Main Function
		try
		{//Trying to connect to the NTP server and pulling any data sent by the server to it
		
			NTPSocket = new Socket("0.ie.pool.ntp.org", 123);//Socket to one of the Irish NTP 
			
			System.out.println("Just connected to " + NTPSocket.getRemoteSocketAddress());//Print a message acknowledging a connection

			InputStream inFromServer = NTPSocket.getInputStream(); //Creating a stream for the input from the server
			DataInputStream in = new DataInputStream(inFromServer); //Getting Data from the input stream
			System.out.println("Server says " + in.readUTF());//Printing out the response from the server
			NTPSocket.close();//Closing socket NTPSocket
		}
		catch(IOException e)
		{//If the NTP server fails, itll inform the user and pull the time from the local machine
			System.out.println("Something went wrong with the connection to the NTP server.");
			System.out.println("Pulling time from the local machine");
			
			
			Calendar now = Calendar.getInstance();//Creating an instance of the object calendar to get the time
			int hour = now.get(Calendar.HOUR_OF_DAY); //Pulls the current hour out of the Calender object
			int minute = now.get(Calendar.MINUTE);//Pulls the current hour out of the Calender object
			
			OutTimeString = Integer.toString(hour) + ":" + Integer.toString(minute); //Converts the current time into our standardised format
			
		} // end of catch to catch any sort of exception

		return OutTimeString;
	}//End of the function that pulls data from the NTP server
}//End of TimeClock Class